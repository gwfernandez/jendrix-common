package com.jendrix.common.component.mapper;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

public interface EntityMappeable<T, M> {

	public abstract T toEntity(M model);

	public abstract M toModel(T entity);

	public default List<M> toModelList(Iterable<T> entityList) {
		List<M> modelList = null;
		if (entityList != null) {
			modelList = new ArrayList<M>();
			for (T entity : entityList) {
				modelList.add(toModel(entity));
			}
		}
		return modelList;
	}

	public default Page<M> toModelPage(Page<T> page) {
		List<M> modelList = null;
		if (page != null) {
			modelList = new ArrayList<M>();
			for (T entity : page.getContent()) {
				modelList.add(toModel(entity));
			}
		}
		return new PageImpl<M>(modelList, page.getPageable(), page.getTotalPages());
	}

	public default List<T> toEntityList(Iterable<M> modelList) {
		List<T> entityList = null;
		if (modelList != null) {
			entityList = new ArrayList<T>();
			for (M model : modelList) {
				entityList.add(toEntity(model));
			}
		}
		return entityList;
	}

}
