package com.jendrix.common.service.impl;

import org.springframework.data.repository.CrudRepository;

import com.jendrix.common.component.mapper.EntityMappeable;
import com.jendrix.common.exception.ServiceRestrictionException;

public abstract class CrudRepositoryServiceImpl<T, M> {

	protected abstract CrudRepository<T, Long> getCrudRepository();

	protected abstract EntityMappeable<T, M> getEntityConverter();

	public Iterable<M> findAll() {
		Iterable<T> entityList = getCrudRepository().findAll();
		return getEntityConverter().toModelList(entityList);
	}

	public M findById(Long id) {
		M model = null;
		if (id != null) {
			T entity = getCrudRepository().findById(id).orElse(null);
			model = getEntityConverter().toModel(entity);
		}
		return model;
	}

	public M save(M model) throws ServiceRestrictionException {
		try {
			validateSave(model);
			T entity = getEntityConverter().toEntity(model);
			entity = getCrudRepository().save(entity);
			return getEntityConverter().toModel(entity);
		} catch (ServiceRestrictionException sre) {
			throw sre;
		}
	}

	protected void validateSave(M model) throws ServiceRestrictionException {
	}

	public void delete(Long id) throws ServiceRestrictionException {
		try {
			if (id != null) {
				T entity = getCrudRepository().findById(id).orElse(null);
				validateDelete(entity);
				getCrudRepository().deleteById(id);
			}
		} catch (ServiceRestrictionException sre) {
			throw sre;
		}
	}

	protected void validateDelete(T entity) throws ServiceRestrictionException {
	}
}
