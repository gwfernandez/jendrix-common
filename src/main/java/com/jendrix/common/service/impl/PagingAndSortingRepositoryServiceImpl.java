package com.jendrix.common.service.impl;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.transaction.annotation.Transactional;

public abstract class PagingAndSortingRepositoryServiceImpl<T, M> extends CrudRepositoryServiceImpl<T, M> {

	protected abstract PagingAndSortingRepository<T, Long> getPagingAndSortingRepository();

	protected final CrudRepository<T, Long> getCrudRepository() {
		return getPagingAndSortingRepository();
	}

	@Transactional(readOnly = true)
	public Page<M> findAll(Pageable pageable) {
		Page<T> page = getPagingAndSortingRepository().findAll(pageable);
		return getEntityConverter().toModelPage(page);
	}

}
