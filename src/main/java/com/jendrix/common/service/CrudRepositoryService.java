package com.jendrix.common.service;

import com.jendrix.common.exception.ServiceRestrictionException;

public interface CrudRepositoryService<T, M> {

	public Iterable<M> findAll();

	public M findById(Long id);

	public M save(M model) throws ServiceRestrictionException;

	public void delete(Long id) throws ServiceRestrictionException;

}
