package com.jendrix.common.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface PagingAndSortingRepositoryService<T, M> extends CrudRepositoryService<T, M> {

	public Page<M> findAll(Pageable pageable);
}
