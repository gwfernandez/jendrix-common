package com.jendrix.common.persistence.entity;

import java.io.Serializable;

@SuppressWarnings("serial")
@Deprecated
public abstract class BaseEntity implements Serializable {

	/**
	 */
	public abstract Long getId();

	/**
	 */
	public abstract void setId(Long id);

	/**
	 */
	@Override
	public int hashCode() {
		return getId().hashCode();
	}

	/**
	 */
	@Override
	public boolean equals(Object to) {
		return getId().equals(((BaseEntity) to).getId());
	}

}