package com.jendrix.common.embeddable;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class DateAudit implements Serializable {

	private static final long serialVersionUID = -3420576486679696294L;

	@Column(name = "fechaAlta", nullable = false)
	private Date fechaAlta;

	@Column(name = "fechaUltimaModificacion")
	private Date fechaUltimaModificacion;

	@Column(name = "fechaBaja")
	private Date fechaBaja;

	public Date getFechaAlta() {
		return fechaAlta;
	}

	public void setFechaAlta(Date fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	public Date getFechaUltimaModificacion() {
		return fechaUltimaModificacion;
	}

	public void setFechaUltimaModificacion(Date fechaUltimaModificacion) {
		this.fechaUltimaModificacion = fechaUltimaModificacion;
	}

	public Date getFechaBaja() {
		return fechaBaja;
	}

	public void setFechaBaja(Date fechaBaja) {
		this.fechaBaja = fechaBaja;
	}
}
