package com.jendrix.common.exception;

import java.util.List;

@SuppressWarnings("serial")
public final class ServiceRestrictionException extends BaseException {

	public ServiceRestrictionException(List<String> messages) {
		super(messages);
	}

	public ServiceRestrictionException(final String message) {
		super(message);
	}
}