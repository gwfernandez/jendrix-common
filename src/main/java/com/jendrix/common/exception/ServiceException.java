package com.jendrix.common.exception;

@SuppressWarnings("serial")
public final class ServiceException extends Exception {

	public ServiceException(String message) {
		super(message);
	}
}