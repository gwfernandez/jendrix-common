package com.jendrix.common.exception;

@SuppressWarnings("serial")
public final class SystemException extends Exception {

	public SystemException(Throwable t) {
		super(t);
	}
}