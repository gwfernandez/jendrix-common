package com.jendrix.common.exception;

@SuppressWarnings("serial")
public final class DataAccessExcepcion extends Exception {

	public DataAccessExcepcion(Throwable t) {
		super(t);
	}
}