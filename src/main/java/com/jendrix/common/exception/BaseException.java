package com.jendrix.common.exception;

import java.util.ArrayList;
import java.util.List;

import com.jendrix.common.util.Checker;

@SuppressWarnings("serial")
public abstract class BaseException extends Exception {
	
	// Properties.
	private List<String> messages;

	public BaseException(List<String> messages) {
		this.messages = messages;
	}

	public BaseException(String message) {
		this.messages = new ArrayList<String>();
		this.messages.add(message);
	}

	public final String getMessage() {
		return getMessagesAsString();
	}


	public final List<String> getMessages() {
		return messages;
	}
	

	public final String getMessagesAsString() {
		StringBuilder sb = new StringBuilder();
		String ls = System.getProperty("line.separator");

		if (!Checker.isEmpty(messages)) {
			for (String msg : messages) {
				if (!Checker.isEmpty(sb.toString())) {
					sb.append(ls);
				}
				sb.append(msg);
			}
		}
		return sb.toString();
	}
}