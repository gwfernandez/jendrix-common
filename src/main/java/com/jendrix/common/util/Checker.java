package com.jendrix.common.util;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.Map;

public final class Checker {
	/**
	 */
	public static final boolean isEmpty(String value) {
		return (value == null || value.trim().length() == 0);
	}

	/**
	 */
	public static final boolean isEmpty(Short value) {
		return (value == null || value.shortValue() == 0);
	}

	/**
	 */
	public static final boolean isEmpty(Integer value) {
		return (value == null || value.intValue() == 0);
	}

	/**
	 */
	public static final boolean isEmpty(Long value) {
		return (value == null || value.longValue() == 0);
	}

	/**
	 */
	public static final boolean isEmpty(Double value) {
		return (value == null || value.doubleValue() == 0);
	}

	/**
	 */
	public static final boolean isEmpty(BigDecimal value) {
		return (value == null || value.doubleValue() == 0);
	}

	/**
	 */
	public static final boolean isEmpty(Date date) {
		return (date == null);
	}

	/**
	 */
	public static final boolean isEmpty(byte[] value) {
		return (value == null || value.length == 0);
	}

	/**
	 */
	@SuppressWarnings("rawtypes")
	public static final boolean isEmpty(Collection value) {
		return (value == null || value.isEmpty());
	}

	/**
	 */
	@SuppressWarnings("rawtypes")
	public static final boolean isEmpty(Map value) {
		return (value == null || value.isEmpty());
	}

	/**
	 */
	public static final boolean isEmpty(Object object) {
		return (object == null);
	}

	// TODO: isModelEmpty() -> analizar si continua este metodo
	// /**
	// */
	// public static final boolean isModelEmpty(BaseEntity entity) {
	// return (entity == null || isEmpty(entity.getId()));
	// }

	/**
	 */
	public static final boolean isPositive(BigDecimal value) {
		return !isEmpty(value) && value.compareTo(BigDecimal.ZERO) > 0;
	}

	/**
	 */
	public static final boolean isNegative(BigDecimal value) {
		return !isEmpty(value) && value.compareTo(BigDecimal.ZERO) < 0;
	}
}